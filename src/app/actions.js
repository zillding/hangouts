import { stringify } from 'qs'
import { Map, List } from 'immutable'
import io from 'socket.io-client'
import fetch from 'isomorphic-fetch'

import { SEARCH_API, API_KEY } from './config'
import { getVideoIndex, getNextVideoId, getPreviousVideoId } from './utils'

export function search(text) {
  return dispatch => {
    dispatch({ type: 'SEARCH' })

    const searchParams = {
      part: 'snippet',
      key: API_KEY,
      q: text,
      type: 'video',
      maxResults: 30
    }
    const url = `${SEARCH_API}?${stringify(searchParams)}`

    fetch(url, {
      mode: 'cors'
    })
    .then(res => {
      if (res.ok) {
        return res.json()
      }

      throw res
    })
    .then(data => dispatch(setSearchResult(data.items)))
    .catch(err => dispatch(setSearchError(err)))
  }
}

export function setSearchTerm(text) {
  return { type: 'SET_SEARCH_TERM', text }
}

function setSearchResult(data) {
  return { type: 'SET_SEARCH_RESULT', data }
}

function setSearchError(error) {
  return { type: 'SET_SEARCH_ERROR', error}
}

export function setUpSocket() {
  return (dispatch, getState) => {
    const socket = io()
    dispatch({ type: 'SET_SOCKET', socket })

    socket.on('connect', () => {
      dispatch(setConnected(true))

      const { roomName } = getState()
      if (roomName) {
        dispatch(sendRoomName(roomName))
        dispatch(notify({
          message: 'Connect to server!',
          level: 'success',
        }))
      }
    })

    socket.on('action', msg => {
      dispatch(notify({
        message: `Action performed: ${msg.type}`,
        level: 'info',
      }))

      switch (msg.type) {
        case 'ADD_VIDEO':
          return dispatch(addVideo(msg.data))
        case 'DELETE_VIDEO':
          return dispatch(deleteVideo(msg.data))
        case 'PLAY':
          return dispatch(play(msg.data))
        case 'PLAY_NEXT':
          return dispatch(playNext())
        case 'PLAY_PREVIOUS':
          return dispatch(playPrevious())
        case 'PAUSE':
          return dispatch(pause())
        case 'RESUME':
          return dispatch(resume())
        case 'SYNC_TIME':
          return dispatch(syncTime(msg.data))
        default:
          return
      }
    })

    socket.on('welcome', roomName => {
      dispatch({ type: 'ROOMNAME', roomName })
    })

    socket.on('start youtube', msg => dispatch(setRoomState(msg)))

    socket.on('disconnect', () => {
      dispatch(setConnected(false))
      dispatch(notify({
        message: 'Lost connection to server!',
        level: 'error',
      }))
      dispatch(pause())
    })
  }
}

function setConnected(connected) {
  return { type: 'SET_CONNECTED', connected }
}

function setRoomState(data) {
  return { type: 'SET_ROOM_STATE', data }
}

export function sendRoomName(roomName) {
  return (dispatch, getState) => {
    const { socket } = getState()

    if (socket.connected) {
      socket.emit('new user', roomName)
    }

    dispatch(setUpWebRTC(roomName))
  }
}

export function sendAction(action, data) {
  return (dispatch, getState) => {
    const { socket } = getState()

    if (socket.connected) {
      dispatch({ type: `SEND_${action}` })
      socket.emit('action', { type: action, data })
    }
  }
}

function addVideo(data) {
  return (dispatch, getState) => {
    dispatch({ type: 'ADD_VIDEO', data })

    const { roomState, playerState } = getState()
    const playlist = roomState.get('playlist')
    if (playlist.size === 1) {
      const nextVideoId = getNextVideoId(playlist, playerState.get('videoId'))
      dispatch(sendAction('PLAY', nextVideoId))
    }
  }
}

function deleteVideo(index) {
  return (dispatch, getState) => {
    const { roomState, playerState } = getState()
    const playlist = roomState.get('playlist')
    const currentPlayingVideoId = playerState.get('videoId')
    const currentVideoIndex = getVideoIndex(playlist, currentPlayingVideoId)
    const nextVideoId = getNextVideoId(playlist, currentPlayingVideoId)

    dispatch({ type: 'DELETE_VIDEO', index })

    if (currentVideoIndex === index) {
      dispatch(sendAction('PLAY', nextVideoId))
    }
  }
}

function play(videoId) {
  return (dispatch, getState) => {
    dispatch({ type: 'PLAY', videoId })

    const { audioIsMuted } = getState()

    if ((videoId && !audioIsMuted) ||
      (!videoId && audioIsMuted)) {
      dispatch(toggleMute())
    }
  }
}

function pause() {
  return (dispatch, getState) => {
    dispatch({ type: 'PAUSE' })

    const { audioIsMuted } = getState()

    if (audioIsMuted) {
      dispatch(toggleMute())
    }
  }
}

function resume() {
  return (dispatch, getState) => {
    dispatch({ type: 'RESUME' })

    const { audioIsMuted } = getState()

    if (!audioIsMuted) {
      dispatch(toggleMute())
    }
  }
}

function playNext() {
  return (dispatch, getState) => {
    const { roomState, playerState } = getState()
    const nextVideoId = getNextVideoId(
      roomState.get('playlist'),
      playerState.get('videoId')
    )
    dispatch({ type: 'PLAY_NEXT', nextVideoId })
  }
}

function playPrevious() {
  return (dispatch, getState) => {
    const { roomState, playerState } = getState()
    const previousVideoId = getPreviousVideoId(
      roomState.get('playlist'),
      playerState.get('videoId')
    )
    dispatch({ type: 'PLAY_PREVIOUS', previousVideoId })
  }
}

function syncTime(time) {
  return (dispatch, getState) => {
    const { player } = getState()
    player.seekTo(time)

    dispatch({ type: 'SYNC_TIME' })
  }
}

export function setPlayer(player) {
  return { type: 'SET_PLAYER', player }
}

export function toggleSidebar() {
  return { type: 'TOGGLE_SIDEBAR' }
}

export function toggleSearch() {
  return { type: 'TOGGLE_SEARCH' }
}

export function setNotificationSystem(ns) {
  return { type: 'SET_NOTIFICATION_SYSTEM', ns }
}

function notify(msg) {
  return (dispatch, getState) => {
    const { notificationSystem } = getState()
    const notification = Object.assign({
      position: 'br'
    }, msg)
    notificationSystem.addNotification(notification)
  }
}

export function setWebRTC(webrtc) {
  return { type: 'SET_WEBRTC', webrtc }
}

function setUpWebRTC(roomName) {
  return (dispatch, getState) => {
    const { webrtc } = getState()

    // webrtc.on('readyToCall', () => webrtc.joinRoom(roomName))
    webrtc.joinRoom(roomName)

    webrtc.on('videoAdded', (video, peer) => {
      dispatch(addPeerVideo({ video, peer }))
    })

    webrtc.on('videoRemoved', (video, peer) => {
      dispatch(removePeerVideo({ video, peer }))
    })
  }
}

function addPeerVideo(data) {
  return (dispatch, getState) => {
    const { peerVideos } = getState()

    if (peerVideos.size === 0) {
      dispatch(selectPeerVideo(data.peer.id))
    }

    dispatch({ type: 'ADD_PEER_VIDEO', data })
  }
}

function removePeerVideo(data) {
  return (dispatch, getState) => {
    const { peerVideos, selectedPeerVideoId } = getState()

    if (selectedPeerVideoId === data.peer.id) {
      const o = peerVideos.find(({ peer }) =>
        peer.id !== selectedPeerVideoId
      )
      const id = o ? o.peer.id : ''
      dispatch(selectPeerVideo(id))
    }

    dispatch({ type: 'REMOVE_PEER_VIDEO', data })
  }
}

export function selectPeerVideo(id) {
  return { type: 'SELECT_PEER_VIDEO', id }
}

export function setApp(app) {
  return { type: 'SET_APP', app }
}

export function toggleMute() {
  return (dispatch, getState) => {
    const { webrtc, audioIsMuted } = getState()
    if (audioIsMuted) {
      webrtc.unmute()
    } else {
      webrtc.mute()
    }
    dispatch({ type: 'TOGGLE_MUTE' })
  }
}

export function toggleVideo() {
  return (dispatch, getState) => {
    const { webrtc, videoIsPaused } = getState()
    if (videoIsPaused) {
      webrtc.resumeVideo()
    } else {
      webrtc.pauseVideo()
    }
    dispatch({ type: 'TOGGLE_VIDEO' })
  }
}

export function setVolume(volume) {
  return { type: 'SET_VOLUME', volume }
}
