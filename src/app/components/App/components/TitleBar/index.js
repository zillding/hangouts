import { Component } from 'react'
import { connect } from 'react-redux'

import { setApp, toggleMute, toggleVideo, toggleSidebar } from 'actions'

import VideoItem from './components/VideoItem'
import YoutubeItem from './components/YoutubeItem'
import ConnectingItem from './components/ConnectingItem'
import VolumeItem from './components/VolumeItem'
import ToggleMuteItem from './components/ToggleMuteItem'
import ToggleVideoItem from './components/ToggleVideoItem'
import ToggleSidebarItem from './components/ToggleSidebarItem'

class TitleBar extends Component {
  render() {
    const {
      currentApp,
      isConnected,
      roomName,
      volume,
      audioIsMuted,
      videoIsPaused,
      showSidebar,
      setApp,
      toggleMute,
      toggleVideo,
      toggleSidebar,
    } = this.props

    return (
      <div className="ui large secondary pointing menu">
        <div className="item">
          Hangouts!
        </div>
        <div className="item">
          <i className="home icon"></i>
          {roomName}
        </div>
        <VideoItem
          currentApp={currentApp}
          onClick={() => setApp('video')} />
        <YoutubeItem
          currentApp={currentApp}
          onClick={() => setApp('youtube')} />
        <div className="right menu">
          {
            isConnected ? null : <ConnectingItem/>
          }
          {
            audioIsMuted ? null : <VolumeItem volume={volume} />
          }
          <ToggleMuteItem
            audioIsMuted={audioIsMuted}
            onClick={toggleMute} />
          <ToggleVideoItem
            videoIsPaused={videoIsPaused}
            onClick={toggleVideo} />
          {
            currentApp === 'youtube' ?
              <ToggleSidebarItem
                showSidebar={showSidebar}
                onClick={toggleSidebar} /> :
              null
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentApp: state.currentApp,
    isConnected: state.isConnected,
    roomName: state.roomName,
    volume: state.volume,
    audioIsMuted: state.audioIsMuted,
    videoIsPaused: state.videoIsPaused,
    showSidebar: state.showSidebar,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setApp: app => dispatch(setApp(app)),
    toggleMute: () => dispatch(toggleMute()),
    toggleVideo: () => dispatch(toggleVideo()),
    toggleSidebar: () => dispatch(toggleSidebar()),
  }
}

const C = connect(
  mapStateToProps,
  mapDispatchToProps
)(TitleBar)

export default C
