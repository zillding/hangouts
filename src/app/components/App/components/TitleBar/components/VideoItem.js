const VideoItem = ({ currentApp, onClick }) => {
  const cn = currentApp === 'video' ?
    'active green item' :
    'item'

  return (
    <a
      className={cn}
      onClick={onClick}>
      <i className="record icon"></i>
      Video
    </a>
  )
}

export default VideoItem
