const YoutubeItem = ({ currentApp, onClick }) => {
  const cn = currentApp === 'youtube' ?
    'active red item' :
    'item'

  return (
    <a
      className={cn}
      onClick={onClick}>
      <i className="youtube play icon"></i>
      YouTube
    </a>
  )
}

export default YoutubeItem
