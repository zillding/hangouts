const ToggleSidebarItem = ({ showSidebar, onClick }) => {
  const cn = showSidebar ?
    'active blue item':
    'item'

  return (
    <a
      className={cn}
      onClick={onClick}>
      <i className="sidebar icon"></i>
      Sidebar
    </a>
  )
}

export default ToggleSidebarItem
