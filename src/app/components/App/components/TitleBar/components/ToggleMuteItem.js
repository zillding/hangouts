const ToggleMuteItem = ({ audioIsMuted, onClick }) => {
  const cn = audioIsMuted ? 'active grey item' : 'item'

  return (
    <a
      className={cn}
      onClick={onClick}>
      <i className="mute icon"></i>
    </a>
  )
}

export default ToggleMuteItem
