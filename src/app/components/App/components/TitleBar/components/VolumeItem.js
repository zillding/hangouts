const containerStyle = {
  minWidth: 110,
  alignSelf: 'center',
  display: 'flex',
  justifyContent: 'flex-end',
}

const ballStyle = {
  marginLeft: 1,
  opacity: 0.7,
}

const VolumeItem = ({ volume }) => {
  const array = []
  for (let i = 0; i < getNumberOfBalls(volume); i++) {
    array.push(1)
  }

  return (
    <div
      className="item"
      style={containerStyle}>
    {
      array.map((value, index) =>
        <div
          key={index}
          className="ui green empty circular label"
          style={ballStyle} />
      )
    }
    </div>
  )
}

export default VolumeItem

function getNumberOfBalls(volume = 0) {
  if (volume < 5) return 1
  if (volume < 9) return 2
  if (volume < 12) return 3
  if (volume < 14) return 4
  return 5
}
