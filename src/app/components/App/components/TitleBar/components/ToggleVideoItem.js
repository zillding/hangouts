const ToggleVideoItem = ({ videoIsPaused, onClick }) => {
  const cn = videoIsPaused ? 'item' : 'active grey item'

  return (
    <a
      className={cn}
      onClick={onClick}>
      <i className="record icon"></i>
    </a>
  )
}

export default ToggleVideoItem
