import { Component } from 'react'
import { connect } from 'react-redux'

const style = {
  position: 'absolute',
  height: '100%',
  width: '100%',
}

const MainVideo = ({ peerVideos, selectedPeerVideoId }) => {
  if (!selectedPeerVideoId) return <div/>

  const o = peerVideos.find(({ peer: { id } }) => id === selectedPeerVideoId)

  if (!o) return <div/>

  const { video } = o

  return (
    <video
      src={video.src}
      id={video.id}
      style={style} />
  )
}

const mapStateToProps = state => {
  return {
    peerVideos: state.peerVideos,
    selectedPeerVideoId: state.selectedPeerVideoId,
  }
}

const C = connect(
  mapStateToProps
)(MainVideo)

export default C
