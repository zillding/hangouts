const style = {
  position: 'absolute',
  top: 5,
  left: 5,
  zIndex: 9999,
  cursor: 'move',
  fontSize: '1.5em',
  fontWeight: 'bold',
  opacity: 0.4,
}

const Handle = () => (
  <i
    className="move icon"
    style={style}>
  </i>
)

export default Handle
