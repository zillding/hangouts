const PeerVideos = ({ data, onSelect }) => (
  <span style={{display: 'flex'}}>
    {
      data.map(({ video, peer }) =>
        <video
          key={peer.id}
          height="150"
          src={video.src}
          id={video.id}
          onClick={() => onSelect(peer.id)} />
      )
    }
  </span>
)

export default PeerVideos
