import { Component } from 'react'
import { connect } from 'react-redux'
import Draggable from 'react-draggable'
import SimpleWebRTC from 'simplewebrtc'

import { setWebRTC, selectPeerVideo, setVolume } from 'actions'

import Handle from './components/Handle'
import PeerVideos from './components/PeerVideos'

const style = {
  position: 'absolute',
  bottom: 0,
  left: 0,
  display: 'flex',
}

class WebRTC extends Component {
  componentDidMount() {
    const { setWebRTC, setVolume } = this.props

    const webrtc = new SimpleWebRTC({
      localVideoEl: this.refs.local,
      remoteVideoEl: '',
      autoRequestMedia: true,
      detectSpeakingEvents: true,
    })

    webrtc.on('volumeChange', (volume, treshold) => {
      if (volume > -45 && volume < -20) {
        setVolume(volume + 45)
      }
    })

    setWebRTC(webrtc)
  }

  render() {
    const {
      currentApp,
      peerVideos,
      selectedPeerVideoId,
      selectPeerVideo,
    } = this.props

    const data = currentApp === 'video' ?
      peerVideos.filterNot(
        ({ peer: { id } }) => id === selectedPeerVideoId
      ) :
      peerVideos

    return (
      <Draggable
        bounds="parent"
        handle=".move.icon">
        <div style={style}>
          <Handle/>
          <video
            ref="local"
            height="150"
          />
          <PeerVideos
            data={data}
            onSelect={selectPeerVideo} />
        </div>
      </Draggable>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentApp: state.currentApp,
    peerVideos: state.peerVideos,
    selectedPeerVideoId: state.selectedPeerVideoId,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setWebRTC: webrtc => dispatch(setWebRTC(webrtc)),
    selectPeerVideo: id => dispatch(selectPeerVideo(id)),
    setVolume: volume => dispatch(setVolume(volume)),
  }
}

const C = connect(
  mapStateToProps,
  mapDispatchToProps
)(WebRTC)

export default C
