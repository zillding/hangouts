export const ResumeButton = ({ isSending, disabled, onResume }) => {
  const cn = isSending ?
    'ui disabled blue basic button loading' :
    'ui blue basic button'

  return (
    <button
      className={cn}
      disabled={isSending || disabled}
      onClick={onResume}>
      Resume
    </button>
  )
}

export const PauseButton = ({ isSending, disabled, onPause }) => {
  const cn = isSending ?
    'ui disabled blue basic button loading' :
    'ui blue basic button'

  return (
    <button
      className={cn}
      disabled={isSending || disabled}
      onClick={onPause}>
      Pause
    </button>
  )
}

export const PrevButton = ({ isSending, disabled, onPrevious }) => {
  const cn = isSending ?
    'ui disabled blue basic button loading' :
    'ui blue basic button'

  return (
    <button
      className={cn}
      disabled={isSending || disabled}
      onClick={onPrevious}>
      Previous
    </button>
  )
}

export const NextButton = ({ isSending, disabled, onNext }) => {
  const cn = isSending ?
    'ui disabled blue basic button loading' :
    'ui blue basic button'

  return (
    <button
      className={cn}
      disabled={isSending || disabled}
      onClick={onNext}>
      Next
    </button>
  )
}

export const SyncButton = ({ isSending, disabled, onSync }) => {
  const cn = isSending ?
    'ui disabled blue basic button loading' :
    'ui blue basic button'

  return (
    <button
      className={cn}
      disabled={isSending || disabled}
      style={{margin: 0}}
      onClick={onSync}>
      Sync Play Time
    </button>
  )
}
