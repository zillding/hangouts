const containerStyle = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
}

const Waiting = () => (
  <div style={containerStyle}>
    <h1 className="ui header">
      <i className="users icon"></i>
      <div className="content">
        Waiting for other users to join...
      </div>
    </h1>
  </div>
)

export default Waiting
