import { Component } from 'react'
import { connect } from 'react-redux'

import { sendRoomName, setNotificationSystem } from 'actions'

import TitleBar from './components/TitleBar'
import Layout from './components/Layout'
import Splash from './components/Splash'
import Notification from './components/Notification'
import WebRTC from './components/WebRTC'
import Waiting from './components/Waiting'
import MainVideo from './components/MainVideo'

const containerStyle = {
  position: 'fixed',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  display: 'flex',
  flexDirection: 'column',
}

const mainStyle = {
  flex: 1,
  position: 'relative',
}

class App extends Component {
  _getVideo() {
    const {
      numberOfPeers,
      currentApp,
    } = this.props

    if (currentApp !== 'video') return null
    if (numberOfPeers === 0) return <Waiting/>
    return <MainVideo/>
  }

  _getContent() {
    const {
      roomName,
      isSendingRoomName,
      sendRoomName,
    } = this.props

    if (!roomName) {
      return (
        <Splash
          isSendingRoomName={isSendingRoomName}
          onSubmit={sendRoomName} />
      )
    }

    return (
      <div style={containerStyle}>
        <div style={{paddingBottom: 10}}>
          <TitleBar/>
        </div>
        <div style={mainStyle}>
          { this._getVideo() }
          <Layout/>
        </div>
      </div>
    )
  }

  render() {
    const { roomName, setNotificationSystem } = this.props

    return (
      <div style={containerStyle}>
        { this._getContent() }
        <WebRTC/>
        <Notification onReady={setNotificationSystem} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    roomName: state.roomName,
    isSendingRoomName: state.isSendingMap.get('ROOMNAME'),
    numberOfPeers: state.peerVideos.size,
    currentApp: state.currentApp,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    sendRoomName: name => dispatch(sendRoomName(name)),
    setNotificationSystem: ns => dispatch(setNotificationSystem(ns)),
  }
}

const C = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

export default C
