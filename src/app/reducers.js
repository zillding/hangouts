import { Map, List } from 'immutable'
import io from 'socket.io-client'

export function webrtc(state = null, action) {
  switch (action.type) {
    case 'SET_WEBRTC':
      return action.webrtc
    default:
      return state
  }
}

export function audioIsMuted(state = false, action) {
  switch (action.type) {
    case 'TOGGLE_MUTE':
      return !state
    default:
      return state
  }
}

export function volume(state = 0, action) {
  switch (action.type) {
    case 'SET_VOLUME':
      return action.volume
    default:
      return state
  }
}

export function videoIsPaused(state = false, action) {
  switch (action.type) {
    case 'TOGGLE_VIDEO':
      return !state
    default:
      return state
  }
}

export function peerVideos(state = List(), action) {
  switch (action.type) {
    case 'ADD_PEER_VIDEO':
      return state.push(action.data)
    case 'REMOVE_PEER_VIDEO':
      const index = state.findIndex(({ video, peer }) =>
        peer.id === action.data.peer.id
      )
      return state.delete(index)
    default:
      return state
  }
}

export function selectedPeerVideoId(state = '', action) {
  switch (action.type) {
    case 'SELECT_PEER_VIDEO':
      return action.id
    default:
      return state
  }
}

export function currentApp(state = 'video', action) {
  switch (action.type) {
    case 'SET_APP':
      return action.app
    default:
      return state
  }
}

export function player(state = null, action) {
  switch (action.type) {
    case 'SET_PLAYER':
      return action.player
    default:
      return state
  }
}

export function socket(state = null, action) {
  switch (action.type) {
    case 'SET_SOCKET':
      return action.socket
    default:
      return state
  }
}

export function isConnected(state = false, action) {
  switch(action.type) {
    case 'SET_CONNECTED':
      return action.connected
    default:
      return state
  }
}

export function notificationSystem(state = null, action) {
  switch (action.type) {
    case 'SET_NOTIFICATION_SYSTEM':
      return action.ns
    default:
      return state
  }
}

export function showSidebar(state = true, action) {
  switch (action.type) {
    case 'TOGGLE_SIDEBAR':
      return !state
    default:
      return state
  }
}

export function showSearch(state = false, action) {
  switch (action.type) {
    case 'SET_ROOM_STATE':
      if (action.data.playlist.length === 0) return true
      return false
    case 'TOGGLE_SEARCH':
      return !state
    default:
      return state
  }
}

export function isSearching(state = false, action) {
  switch (action.type) {
    case 'SEARCH':
      return true
    case 'SET_SEARCH_RESULT':
      return false
    case 'SET_SEARCH_ERROR':
      return false
    default:
      return state
  }
}

export function searchTerm(state = '', action) {
  switch (action.type) {
    case 'SET_SEARCH_TERM':
      return action.text
    default:
      return state
  }
}

export function searchResult(state = [], action) {
  switch (action.type) {
    case 'SET_SEARCH_RESULT':
      return action.data
    default:
      return state
  }
}

export function searchError(state = null, action) {
  switch (action.type) {
    case 'SET_SEARCH_ERROR':
      return action.error
    default:
      return state
  }
}

export function isSendingMap(state = Map(), action) {
  const { type } = action

  // send actions
  const actionArray = [
    'ROOMNAME',
    'ADD_VIDEO',
    'DELETE_VIDEO',
    'PLAY',
    'PLAY_NEXT',
    'PLAY_PREVIOUS',
    'PAUSE',
    'RESUME',
    'SYNC_TIME',
  ]
  const sendActionArray = actionArray.map(s => `SEND_${s}`)

  if (actionArray.indexOf(type) >= 0) {
    return state.set(type, false)
  }

  if (sendActionArray.indexOf(type) >= 0) {
    const act = type.substring(5)
    return state.set(act, true)
  }

  return state
}

export function roomName(state = '', action) {
  switch (action.type) {
    case 'ROOMNAME':
      return action.roomName
    default:
      return state
  }
}

const defaultRoomState = Map({
  playlist: List(),
})
export function roomState(state = defaultRoomState, action) {
  switch (action.type) {
    case 'SET_ROOM_STATE':
      const { playlist } = action.data
      return Map({
        playlist: List(playlist)
      })
    case 'ADD_VIDEO':
      return state.set('playlist',
        state.get('playlist').push(action.data))
    case 'DELETE_VIDEO':
      return state.set('playlist',
        state.get('playlist').delete(action.index))
    default:
      return state
  }
}

const defaultPlayerState = Map({
  videoId: '',
  isPlaying: false,
})
export function playerState(state = defaultPlayerState, action) {
  switch (action.type) {
    case 'PLAY':
      const { videoId } = action
      return state.set('videoId', videoId).set('isPlaying', videoId ? true : false)
    case 'PLAY_NEXT':
      return state.set('videoId', action.nextVideoId)
    case 'PLAY_PREVIOUS':
      return state.set('videoId', action.previousVideoId)
    case 'PAUSE':
      return state.set('isPlaying', false)
    case 'RESUME':
      return state.set('isPlaying', true)
    default:
      return state
  }
}
