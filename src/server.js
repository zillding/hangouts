import express from 'express'
import { Server } from 'http'
import SocketIO from 'socket.io'
import { List } from 'immutable'
import md5 from 'md5'

import { getNumberOfUsersInRoom } from './utils'

//////////////////////////////////////////////
// Global state data
const rooms = {}

function initRoom(name) {
  rooms[name] = {
    playlist: List(),
    currentPlayingVideoId: '',
  }
}

function deleteRoom(name) {
  delete rooms[name]
}

// mutate the global state
function updateData(room, field, data) {
  rooms[room][field] = data
}
//////////////////////////////////////////////

const app = express()
const http = Server(app)
const io = SocketIO(http)

io.on('connection', socket => {
  let room = 'default_room'

  socket.on('new user', name => {
    room = name.trim() || room

    socket.join(room)

    // send initial data
    if (!rooms[room]) {
      initRoom(room)
    }

    socket.emit('welcome', room)
  })

  socket.on('start youtube', () => {
    const { playlist, currentPlayingVideoId } = rooms[room]

    socket.emit('start youtube', {
      playlist: playlist.toArray()
    })

    socket.emit('action', {
      type: 'PLAY',
      data: currentPlayingVideoId
    })
  })

  socket.on('action', msg => {
    io.in(room).emit('action', msg)

    // store on server
    let field = 'playlist'
    switch (msg.type) {
      case 'ADD_VIDEO':
        return updateData(room, field, rooms[room][field].push(msg.data))
      case 'DELETE_VIDEO':
        return updateData(room, field, rooms[room][field].delete(msg.data))
      case 'PLAY':
        field = 'currentPlayingVideoId'
        return updateData(room, field, msg.data)
      default:
        return
    }
  })

  socket.on('disconnect', () => {
    // clean up data
    if (getNumberOfUsersInRoom(io, room) === 0) {
      deleteRoom(room)
    }
  })
})

const PORT = 3000
http.listen(PORT, err => {
  if (err) {
    console.log(err)
    return
  }

  console.log(`Listening on port: ${PORT}`)
})

export default app
